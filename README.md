This is a repository of example wrappers and scripts to make TI Backlog API requests.

Please refer to [API Support](https://api-portal.ti.com/support) for additional help and resources.

# 1. Python API Requests

The file `Example_Script` under the `python` directory is a program that automatically runs through each of the backlog API calls, using the following sequence of methods:

1. `__create_quote(self)`: Creates a backlog quote.
2. `__retrieve_quote(self)`: Retrieves the backlog quote created in Step #1.
3. `__create_order(self)`: Creates a (test) backlog order.
4. `__retrieve_order(self)`: Retrieves the backlog order created in Step #3.
5. `__change_order(self)`: Changes the backlog order created in Step #3 by inserting a new line item.
6. `__retrieve_invoice(self)`: Retrieves the invoice of the order created in Step #3.
7. `__retrieve_asn(self)`: Retrieves the Advance Ship Notice (ASN) of the order created in Step #3.
8. `__post_remittance(self)`: Posts remittance advice for the invoice in Step #6.

Steps 6-8 are disabled because they require the order to be first processed by TI's systems. However, the code is given there as an example for your reference. After updating the user credentials in the script (lines 341-347), the example script can be run in the terminal using the following command:

```
py Example_Script.py
``` 

The example script uses a module `TI_Backlog_API_V2` of API-specific wrapper classes in order to interface with the TI Backlog API v2 suite. This module is also available in the `python` directory in order to be used by customers out-of-the-box.

- `TI_Backlog_ASN` retrieves Advance Ship Notices (ASN) and related information pertaining to a backlog order.
- `TI_Backlog_Invoice` retrieves financial documents pertaining to a backlog order.
- `TI_Backlog_Orders` allows the user to place, view, and edit backlog orders.
- `TI_Backlog_Remittance` allows the user to post remittance advice for a given invoice.
- `TI_Backlog_Quotes` allows the user to post and retrieve quotes prior to backlog orders.

You can import specific classes from the above module using the following code in Python, replacing `<class>` with your desired API wrapper class:

```
from TI_Backlog_API import <class>
```

Finally, the `API_Accessor.py` module contains the back-end logic for each of the API classes to acquire an API access token and send API requests.

The markdown file `python/Tutorial.md` goes into greater detail about how to write your own programs and classes to interface with APIs in Python.

You must have installed the `requests` package to use any of these classes and modules; it is available freely under the Apache2 license. The programs were written using Python 3.7.3.
