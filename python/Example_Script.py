import json
import os
from datetime import datetime, timedelta
from TI_Backlog_API_V2 import TI_Backlog_Orders, TI_Backlog_Quotes, TI_Backlog_Remittance, TI_Backlog_ASN, TI_Backlog_Invoice
import textwrap

class Backlog_Example_Script():

    def __init__(self, server, clientID, clientSecret, shipTo, soldTo, payerID, debug=False, opn='SN74LS00N'):

        self.server = server
        self.clientID = clientID
        self.clientSecret = clientSecret
        self.shipTo = shipTo
        self.soldTo = soldTo
        self.payerID = payerID
        self.opn1 = opn
        self.debug = debug

    def execute(self):

        """
        Executes the test procedure based on given credentials.
        """

        # Get current datetime and generate test ID for order and quote.
        self.now = datetime.now()
        self.testID = self.now.strftime("TEST%Y%m%d%H%M%S")

        # Print test header.
        print("Customer sold-to ID is {}.".format(self.soldTo))
        print("API authentication ID is {}.".format(self.clientID))
        print("Test ID for order and quote is {}.".format(self.testID))

        # Create list of test step functions to run through iteratively.
        steps = [
            self.__create_wrapper_objects,
            self.__create_quote, self.__retrieve_quote,
            self.__create_order, self.__retrieve_order, 
            self.__change_order, self.__retrieve_invoice, 
            self.__retrieve_asn, self.__post_remittance
        ]

        # Perform each step.
        for step in steps:

            # Retrieve test result code and response.
            # Code is 0 if successful, or 1 if failed.
            failure = step()

            # If code indicates failure (1):
            if(failure):
                
                # Indicate that procedure will terminate.
                print("     Test procedure will terminate now.")

                # Exit with failure code.
                exit(1)

        # Indicate that procedure has successfully been completed.
        print("Test procedure completed successfully.")

    def __create_wrapper_objects(self):

        """
        Create wrapper objects for backlog Orders, Quotes, Invoice, and Remittance API.
        """

        # Wrapper for orders API.
        self.ordersAPI = TI_Backlog_Orders(
            server = self.server,
            client_id=self.clientID,
            client_secret=self.clientSecret
        )

        # Wrapper for quotes API.
        self.quotesAPI = TI_Backlog_Quotes(
            server = self.server,
            client_id=self.clientID,
            client_secret=self.clientSecret
        )

        # Wrapper for invoice API.
        self.invoice = TI_Backlog_Invoice(
            server = self.server,
            client_id=self.clientID,
            client_secret=self.clientSecret
        )

        # Wrapper for invoice API.
        self.asn = TI_Backlog_ASN(
            server = self.server,
            client_id=self.clientID,
            client_secret=self.clientSecret
        )

        # Wrapper for remittance API.
        self.remittance = TI_Backlog_Remittance(
            server = self.server,
            client_id=self.clientID,
            client_secret=self.clientSecret
        )

        # Return success code 0 and empty dictionary (usually JSON server response).
        return 0

    def __create_quote(self):

        """
        Create backlog quote.
        """

        # Add item to quote.
        self.quotesAPI.add_item_to_quote(
            part_number='SN74LS00N',
            quantity=10000
        )

        # Place quote.
        response = self.quotesAPI.post_quote(
            customer_name='Test Customer'
        )

        # Retrieve quote ID.
        self.__quote_number = response.json()['quotes'][0]['quoteNumber']

        # Analyze result.
        outcome = self.get_result(response=response)

        # Print response and elapsed time.
        print("  1. Create Quote: {} ({} ms).".format(
            outcome,
            int(response.elapsed.total_seconds() * 1000)
        ))

        # Return outcome code and response JSON.
        return outcome == "FAIL"

    def __retrieve_quote(self):

        """
        Retrieve backlog quote that has been created.
        """

        # Retrieve existing quote.
        response = self.quotesAPI.get_quote(
            quote_number=self.__quote_number
        )
        outcome = self.get_result(response=response)

        # Print outcome and elapsed time.
        print("  2. Retrieve Quote: {} ({} ms).".format(
            outcome,
            int(response.elapsed.total_seconds() * 1000)
        ))

        # Return outcome code and response JSON.
        return outcome == "FAIL"

    def __create_order(self):

        """
        Create a backlog order.
        """

        # Define line item to be ordered.
        self.ordersAPI.add_item_to_cart(
            part_number='SN7407N',
            quantity=10000,
            delivery_date=datetime(
                year=2027,
                month=6,
                day=15
            ),
            unit_price=0.30,
            currency_code='USD'
        )

        # Send HTTP request and retrieve outcome.
        response = self.ordersAPI.post_order(
            customer_name='Test Customer',
            customer_order_number=self.testID,
            ship_to=self.shipTo
        )
        outcome = self.get_result(response=response)

        # Print outcome and elapsed time in milliseconds.
        print("  3. Create Order: {} ({} ms).".format(
            outcome,
            int(response.elapsed.total_seconds() * 1000)
        ))

        # Store supplier order ID from server response.
        self.supplierOrderID = "MISSING"
        if(outcome == "PASS"):
            self.supplierOrderID = response.json()['orders'][0]['orderNumber']
        print("     Supplier order # is {}.".format(self.supplierOrderID))

        # Return true only if outcome is FAIL; also return server response.
        return outcome == "FAIL"

    def __retrieve_order(self):

        """
        Retrieve backlog order that has been created.
        """

        # Attempt to retrieve order info.
        response = self.ordersAPI.retrieve_order_by_customer_number(
            customerOrderNumber=self.testID
        )
        outcome = self.get_result(response=response)

        # Print outcome and elapsed time.
        print("  4. Retrieve Order: {} ({} ms).".format(
            outcome,
            int(response.elapsed.total_seconds() * 1000)
        ))

        # Return outcome code and response JSON.
        return outcome == "FAIL"

    def __change_order(self):

        """
        Change backlog order that has been created.
        """

        # Define payload; there's not an easy way to represent this API call.
        line_items = [
            {
                "customerLineItemNumber": 1,
                "lineItemChangeIndicator": "U",
                "tiPartNumber": "SN7407N",
                "customerAnticipatedUnitPrice": 0.30,
                "customerCurrencyCode": "USD",
                "schedules": [
                    {
                        "requestedQuantity": 20000,
                        "requestedDeliveryDate": "2027-06-15"
                    }
                ]
            }
        ]

        # Send request and retrieve server response.
        response = self.ordersAPI.change_order(
            customer_name='Test Customer',
            customer_order_number=self.testID,
            ship_to=self.shipTo,
            line_items=line_items
        )
        outcome = self.get_result(response=response)

        # Print outcome and elapsed time.
        print("  5. Change Order: {} ({} ms).".format(
            outcome,
            int(response.elapsed.total_seconds() * 1000)
        ))

        # Return outcome code and response JSON.
        return outcome == "FAIL"

    def __retrieve_invoice(self):

        """
        Retrieve invoice document.
        """
        
        print("  6. Retrieve Invoice: Requires order to have been processed.")
        print("     An example of the API call is given, but disabled, in lines 274-282.")

        # # Send request and retrieve server response.
        # response = self.invoice.retrieve_by_customer_order_number(
        #   order_number=self.testID
        # )
        # outcome = self.get_result(response=response)
        # print("  6. Retrieve Invoice: {} ({} ms).".format(
        #     outcome,
        #     int(response.elapsed.total_seconds() * 1000)
        # ))

        return 0

    def __retrieve_asn(self):

        """
        Retrieve ASN document.
        """

        # STEP 8: Retrieve advance shipment notice (ASN) document.
        print("  7. Retrieve ASN: Requires order to have been processed.")
        print("     An example of the API call is given, but disabled, in lines 296-305.")

        # # The below example assumes we would like to use the customer purchase order ID.
        # # We will use that because it is a self-defined variable we used earlier.
        # response = self.asn.retrieve_by_customer_order_number(
        #     order_number=self.testID
        # )
        # outcome = self.get_result(response=response)
        # print("  7. Post ASN: {} ({} ms).".format(
        #     outcome,
        #     int(response.elapsed.total_seconds() * 1000)
        # ))

        return 0

    def __post_remittance(self):

        """
        Post remittance advice.
        """
        print("  8. Post Remittance: Requires order to have been processed.")
        print("     An example of the API call is given, but disabled, in lines 317-33.")

        # # Define line item.
        # # This requires us to have already run the __retrieve_invoice() function.
        # line_item = {
        #     "paymentAmount": 20000,
        #     "financialDocumentNumber": self.invoiceID
        # }
        # response = self.remittance.post_remittance(
        #     advice_number=self.testID,
        #     line_items=[
        #         line_item
        #     ]
        # )
        # outcome = self.get_result(response=response)
        # print("  8. Post Remittance: {} ({} ms).".format(
        #     outcome,
        #     int(response.elapsed.total_seconds() * 1000)
        # ))

        return 0

    def get_result(self, response, code=200):

        # Determine outcome itself.
        result = 'PASS' if response.status_code == code else 'FAIL'

        # If debug is on, print response.
        if(self.debug):
            print(textwrap.indent(text=response.text, prefix="     "))

        # Return outcome.
        return result

# Our main function.
print("Welcome to the backlog API example script!")
print("We will test a variety of API functions using Python.")
print("Please replace lines 341-347 with your own customer credentials.\n")

backlog_example = Backlog_Example_Script(
    server = 'https://transact-pre.ti.com',
    clientID = 'Put API Key Here',
    clientSecret = 'Put API Secret Here',
    shipTo = 'Put Ship-To ID Here',
    soldTo = 'Put Sold-To ID Here',
    payerID = 'Put Payer ID Number Here',
    debug = False # Set to True to always view server responses.
)

backlog_example.execute()