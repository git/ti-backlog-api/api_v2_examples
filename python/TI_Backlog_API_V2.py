# Copyright (C) 2022 Texas Instruments Incorporated
#
#
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions 
# are met:
#
#   Redistributions of source code must retain the above copyright 
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the 
#   documentation and/or other materials provided with the   
#   distribution.
#
#   Neither the name of Texas Instruments Incorporated nor the names of
#   its contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from API_Accessor import API_Accessor

class TI_Backlog_Invoice:

    """
    API to retrieve invoice documents from TI.
    """

    def retrieve_by_order_number(self, order_number, request_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/financial-documents".format(self.server)
        url += "?orderNumber={}".format(order_number)
        url += "&requestInvoicePDF={}".format('true' if request_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def retrieve_by_customer_order_number(self, order_number, request_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/financial-documents".format(self.server)
        url += "?customerPurchaseOrderNumber={}".format(order_number)
        url += "&requestInvoicePDF={}".format('true' if request_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def retrieve_by_delivery_number(self, delivery_number, request_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/financial-documents".format(self.server)
        url += "?deliveryNumber={}".format(delivery_number)
        url += "&requestInvoicePDF={}".format('true' if request_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def retrieve_by_document_number(self, document_number, request_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/financial-documents".format(self.server)
        url += "?financialDocumentNumber={}".format(document_number)
        url += "&requestInvoicePDF={}".format('true' if request_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def __init__(self, client_id, client_secret, server):

        """
        Creates object through which to interact with TI financial document retrieve API.
        """

        # Store API server. It should be formatted as "https://example.server.com".
        self.server = server

        # Get API access object based on given information: url, client ID, and client secret.
        self.api = API_Accessor(server=server, client_id=client_id, client_secret=client_secret)

class TI_Backlog_ASN:

    """
    API to retrieve advance shipment notices (ASN) from TI.
    """

    def retrieve_by_order_number(self, order_number, request_invoice_pdf=False, request_waybill_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/advanced-shipment-notices".format(self.server)
        url += "?orderNumber={}".format(order_number)
        url += "&requestCommercialInvoicePDF={}".format('true' if request_invoice_pdf else 'false')
        url += "&requestWaybillPDF={}".format('true' if request_waybill_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def retrieve_by_customer_order_number(self, order_number, request_invoice_pdf=False, request_waybill_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/advanced-shipment-notices".format(self.server)
        url += "?customerPurchaseOrderNumber={}".format(order_number)
        url += "&requestCommercialInvoicePDF={}".format('true' if request_invoice_pdf else 'false')
        url += "&requestWaybillPDF={}".format('true' if request_waybill_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def retrieve_by_waybill_number(self, order_number, request_invoice_pdf=False, request_waybill_pdf=False):

        """
        Retrieves invoice for given order and customer information.
        """

        # Create URL with given parameters.
        url = "{}/v2/backlog/advanced-shipment-notices".format(self.server)
        url += "?wayBillNumber={}".format(order_number)
        url += "&requestCommercialInvoicePDF={}".format('true' if request_invoice_pdf else 'false')
        url += "&requestWaybillPDF={}".format('true' if request_waybill_pdf else 'false')

        # Make GET request to given URL.
        return self.api.get(url)

    def __init__(self, client_id, client_secret, server):

        """
        Creates object through which to interact with TI advance shipment notice API.
        """

        # Store API server. It should be formatted as "https://example.server.com".
        self.server = server

        # Get API access object based on given information: url, client ID, and client secret.
        self.api = API_Accessor(server=server, client_id=client_id, client_secret=client_secret)


class TI_Backlog_Remittance:

    """
    API to send remittance advice information to TI.
    """

    def post_remittance(self, advice_number, line_items, currency_code="USD"):

        """
        Send remittance advice to TI. Passes data as a JSON object.
        """

        # Define payload.
        json = {
            "remittanceAdviceNumber": advice_number,
            "currencyCode": currency_code,
            "lineItems": line_items
        }

        # URL onto which to post an order.
        url = "{}/v2/backlog/remittance-advice".format(self.server)

        # Make request from the Apigee access object.
        return self.api.post(url=url, json=json)

    def __init__(self, client_id, client_secret, server):

        """
        Creates object through which to interact with TI remittance advice API.
        """

        # Store API server. It should be formatted as "https://example.server.com".
        self.server = server

        # Get API access object based on given information: url, client ID, and client secret.
        self.api = API_Accessor(server=server, client_id=client_id, client_secret=client_secret)

class TI_Backlog_Quotes:

    """
    API to create and retrieve quotes with TI.
    """

    def add_item_to_quote(self, part_number, quantity):

        self.__line_items.append(
            {
                "tiPartNumber": part_number,
                "quantity":quantity
            }
        )

    def post_quote(self, customer_name):

        """
        Create a quote for a backlog order with TI. Passes data as a JSON object.
        """

        # Indicate issue if no items added to quote.
        if not self.__line_items:
            print("Warning: No items added to cart, so no quote will be created.")
            return {}

        # Define JSON payload.
        json = {
            "quote": {
                "endCustomerCompanyName": customer_name,
                "lineItems": self.__line_items
            }
        }

        # URL onto which to post an order.
        url = "{}/v2/backlog/quotes".format(self.server)

        # Make request from the Apigee access object.
        return self.api.post(url=url, json=json)

    def get_quote(self, quote_number):

        """
        Create a quote for a backlog order with TI. Passes data as a JSON object.
        """

        # URL onto which to post an order.
        url = "{}/v2/backlog/quotes?quoteNumber={}".format(self.server, quote_number)

        # Make request from the Apigee access object.
        return self.api.get(url=url)

    def __init__(self, client_id, client_secret, server):

        """
        Creates object through which to interact with TI quoting API.
        """

        # Store API server. It should be formatted as "https://example.server.com".
        self.server = server

        # Get API access object based on given information: url, client ID, and client secret.
        self.api = API_Accessor(server=server, client_id=client_id, client_secret=client_secret)

        # Create item holder.
        self.__line_items = []

class TI_Backlog_Orders:

    """
    API to place backlog orders with TI.
    """

    def add_item_to_cart(self, part_number, quantity, delivery_date, unit_price, currency_code="USD"):

        """
        Adds item to order cart.
        """

        item_number = len(self.__line_items) + 1

        self.__line_items.append({
            "customerLineItemNumber": item_number,
            "tiPartNumber": part_number,
            "customerAnticipatedUnitPrice": unit_price,
            "customerCurrencyCode": currency_code,
            "schedules": [
                {
                    "requestedQuantity": quantity,
                    "requestedDeliveryDate": delivery_date.strftime('%Y-%m-%d')
                }
            ]
        })

    def post_order(self, customer_name, customer_order_number, ship_to):

        """
        Create a TI backlog order. Passes data as a JSON object.
        """

        # Define payload.
        json = {
            "order": {
                "endCustomerCompanyName": customer_name,
                "customerPurchaseOrderNumber": customer_order_number,
                "shipToAccountNumber": ship_to,
                "lineItems": self.__line_items
            }
        }

        # URL onto which to post an order.
        url = "{}/v2/backlog/orders".format(self.server)

        # Make request from the Apigee access object.
        return self.api.post(url=url, json=json)

    def change_order(self, customer_name, customer_order_number, ship_to, line_items):

        """
        Change a TI backlog order. Passes data through a JSON object.
        """

        # Define payload.
        json = {
            "order": {
                "endCustomerCompanyName": customer_name,
                "customerPurchaseOrderNumber": customer_order_number,
                "shipToAccountNumber": ship_to,
                "lineItems": line_items
            }
        }

        # URL onto which to post an order.
        url = "{}/v2/backlog/orders/changeByCustomerPurchaseOrderNumber".format(self.server)

        # Make request from the Apigee access object.
        return self.api.post(url=url, json=json)

    def retrieve_order_by_supplier_number(self, supplierOrderNumber):

        """
        Retrieve a TI backlog order.
        """

        # URL onto which to post an order.
        url = "{}/v2/backlog/orders?orderNumber={}".format(self.server, supplierOrderNumber)

        # Make request from the Apigee access object.
        return self.api.get(url=url)

    def retrieve_order_by_customer_number(self, customerOrderNumber):

        """
        Retrieve a TI backlog order.
        """

        # URL onto which to post an order.
        url = "{}/v2/backlog/orders?customerPurchaseOrderNumber={}".format(self.server, customerOrderNumber)

        # Make request from the Apigee access object.
        return self.api.get(url=url)

    def __init__(self, client_id, client_secret, server):

        """
        Creates object through which to interact with TI backlog orders APIs.
        """

        # Store API server. It should be formatted as "https://example.server.com".
        self.server = server

        # Get API access object based on given information: url, client ID, and client secret.
        self.api = API_Accessor(server=server, client_id=client_id, client_secret=client_secret)

        # Create container of line items.
        self.__line_items = []